
class ProfilePageLocators
{

    get ProfileOption(){
        return $('//a[@id="ui-id-4"]');
     }
  
     get EditProfileOption(){
        return $('//a[@id="ui-id-5"]');
     }

     get SelectTitle(){
        return $('//select[@id="user_title"]');
     }

     get EnterSurname(){
        return $('//input[@id="user_surname"]');
     }

     get EnterFirstname(){
        return $('//input[@id="user_firstname"]');
     }

     get EnterPhone(){
        return $('//input[@id="user_phone"]');
     }

     get SelectBirthYear(){
        return $('//select[@id="user_dateofbirth_1i"]');
     }

     get SelectBirthMonth(){
        return $('//select[@id="user_dateofbirth_2i"]');
     }

     get SelectBirthDate(){
        return $('//select[@id="user_dateofbirth_3i"]');
     }

     get SelectLicenseType(){
        return $('//input[@id="user_licencetype_t"]');
     }

     get SelectLicensePeriod(){
        return $('//select[@id="user_licenceperiod"]');
     }


     get SelectOccupation(){
        return $('//select[@id="user_occupation_id"]');
     }

     get EnterStreet(){
        return $('//input[@id="user_address_attributes_street"]');
     }


     get EnterCity(){
        return $('//input[@id="user_address_attributes_city"]');
     }

     get EnterCountry(){
        return $('//input[@id="user_address_attributes_county"]');
     }


     get EnterPostcode(){
        return $('//input[@id="user_address_attributes_postcode"]');
     }


     get UpdateUserButton(){
        return $('//input[@value="Update User" and @type="button"]');
     }

     get ModifiedTime(){
        return $('//div[@class="navigation ui-tabs ui-widget ui-widget-content ui-corner-all"]');
     }






}

module.exports=new ProfilePageLocators();