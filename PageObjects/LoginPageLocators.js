
class LoginPageLocators
{

    userName="";
    UserName=function(userName){
        this.userName=userName;
    }
    get EnterEmail(){
        return $('//input[@id="email"]');
     }

     get EnterPassword(){
        return $('//input[@id="password"]');
     }

     get LogIn(){
        return $('//input[@type="submit" and @value="Log in"]');
     }


     get ValidUser(){
        // return $('//h4[text()="'+this.userName+'"]');
        return $('//div[@class="content"]/h4');

     }

     get LogOut(){
        return $('//input[@type="submit" and @value="Log out"]');

     }

     get InvalidUserText(){
        return $('//b[text()="Enter your Email address and password correct"]');

     }



}

module.exports=new LoginPageLocators();