const QuotationPageLocators = require("../PageObjects/QuotationPageLocators");
const LoginPageLocators = require("../PageObjects/LoginPageLocators");
var UniqNo='';
var mileage='';

function makeid(length) {
    let result = '';
    const characters = '987654321';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
}

class QuotationPageActions
{
    

    async UserRequestsQuotation(){
    
        let WaitForRequestQuotationOption=await (QuotationPageLocators.RequestQuotationOption);
        await WaitForRequestQuotationOption.waitForDisplayed({ timeout: 5000 });
        await (QuotationPageLocators.RequestQuotationOption).click();
        console.log("Clicked on request quotation option");
        await (QuotationPageLocators.SelectBreakdownCover).selectByVisibleText("Roadside");
        console.log("Selected Roadside BreakdownCover");
        await (QuotationPageLocators.WindScreenRepairOption).click();
        console.log("Selected No WindScreenRepair Option");
        await (QuotationPageLocators.EnterIncident).click();
        await (QuotationPageLocators.EnterIncident).setValue("accident1");
        console.log("Entered accident1 in Incident option");
        await (QuotationPageLocators.EnterRegistration).click();
        await (QuotationPageLocators.EnterRegistration).setValue("boss1");
        console.log("Entered boss1 in registration option");
        mileage=makeid(5)
        await (QuotationPageLocators.EnterMileage).click();
        await (QuotationPageLocators.EnterMileage).setValue(mileage);
        console.log("Entered "+mileage+ " in registration option");
        await (QuotationPageLocators.EnterVehicleValue).click();
        await (QuotationPageLocators.EnterVehicleValue).setValue("1000");
        console.log("Entered 1000 in Estimation value option");
        await (QuotationPageLocators.SelectParkingLocation).selectByVisibleText("Driveway/Carport");
        console.log("Selected Driveway/Carport in Parking location");
        await (QuotationPageLocators.SelectPolicyStartYear).selectByVisibleText("2020");
        console.log("Selected 2020 as Policy Start Year");
        await (QuotationPageLocators.SelectPolicyStartMonth).selectByVisibleText("June");
        console.log("Selected June as Policy Start Month");
        await (QuotationPageLocators.SelectPolicyStartDate).selectByVisibleText("1");
        console.log("Selected 1 as Policy Start Date");
        await (QuotationPageLocators.SaveQuotation).click();
        console.log("Clicked on Save Quotation option");
        let WaitForIDNO=await (QuotationPageLocators.IdentificationNo);
        await WaitForIDNO.waitForDisplayed({ timeout: 5000 });
        let IdNo=await (QuotationPageLocators.IdentificationNo).getText();
        UniqNo=IdNo.split(":")[1].split("Please")[0].trim();
        console.log("Identification No:"+UniqNo);
        await browser.back();
        await browser.refresh();
    }
    
    async UserRetrievesAndValidatesQuotation(){
        let WaitForRetrieveQuotationOption=await (QuotationPageLocators.RetrieveQuotationOption);
        await WaitForRetrieveQuotationOption.waitForDisplayed({ timeout: 5000 });
        await (QuotationPageLocators.RetrieveQuotationOption).click();
        console.log("Clicked on Retrieve Quotation option");
        await (QuotationPageLocators.EnterIdentificationNo).click();
        console.log("Clicked on identification no option");
        await (QuotationPageLocators.EnterIdentificationNo).setValue(UniqNo);
        console.log("Entered identification no");
        await (QuotationPageLocators.RetrieveOption).click();
        console.log("Clicked on retrieve option");
        await (QuotationPageLocators.ValidateWindScreenRepair).waitForDisplayed();
        await expect(QuotationPageLocators.ValidateWindScreenRepair).toBeDisplayed();
        console.log("Validated WindScreenRepair value");
        await expect(QuotationPageLocators.ValidateRegistration).toBeDisplayed();
        console.log("Validated Registration value");
        await QuotationPageLocators.Mileage(mileage)
        await expect(QuotationPageLocators.ValidateMileage).toBeDisplayed();
        console.log("Validated mileage value");
        await expect(QuotationPageLocators.ValidateEstimatedValue).toBeDisplayed();
        console.log("Validated Estimated value");
        await expect(QuotationPageLocators.ValidateParkingLocation).toBeDisplayed();
        console.log("Validated parking location value");
        await expect(QuotationPageLocators.ValidateStartOfPolicy).toBeDisplayed();
        console.log("Validated start of policy value");
        await expect(QuotationPageLocators.ValidateBreakdownCover).toBeDisplayed();
        console.log("Breakdowncover value is not displayed");
        await browser.back();
        await browser.refresh();
        await (LoginPageLocators.LogOut).click();
        console.log("User clicked on log out button");
    
 

    }
}

module.exports=new QuotationPageActions();