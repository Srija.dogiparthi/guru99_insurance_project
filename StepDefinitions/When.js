
const { Given, When, Then } = require('@wdio/cucumber-framework');
const LoginPageActions = require('../PageActions/LoginPageActions');
const QuotationPageActions = require('../PageActions/QuotationPageActions ');
const ProfilePageActions = require('../PageActions/ProfilePageActions');



Then(/^User log in to application with valid credentials$/, async function () {

    await LoginPageActions.UserLoginToApplicationWithValidCredentials();
});



When(/^User tries to login to application with invalid credentials$/, async function () {
    await LoginPageActions.UserTriesToLoginToApplicationWithInvalidCredentials();

});

When(/^User log in to application with valid credentials and request Quotation$/, async function () {
    await LoginPageActions.UserLoginToApplicationWithValidCredentials();
    await QuotationPageActions.UserRequestsQuotation();

});


When(/^User log in to application with valid credentials and try to edit profile$/, async function () {
    await LoginPageActions.UserLoginToApplicationWithValidCredentials();
    await ProfilePageActions.UserTriesToEditProfilePage();

});


 
 
